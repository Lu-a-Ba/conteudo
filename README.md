# Conteúdo

Conteúdo das páginas do Lu-a-Bá

## O que é o Lu-a-Bá?

O projeto **Lu-a-Bá** é uma coletânea de informações sobre o ambiente de
desenvolvimento [Lua](https://lua.org), estruturado e pensado para o público
brasileiro, embora possa ser igualmente útil para todo o público lusófono.

O nome foi inspirado no termo *bê-a-bá* usado em referência ao processo
de alfabetização. O que se pretende com esse projeto é uma forma de
letramento, porém na linguagem Lua.

Portanto, o Lu-a-Bá é um projeto educativo.

## Escopo do projeto

Objetivamente, o projeto contará com os seguintes recursos:

* [**Tutorial**](documentos/tutoriais/index.md): um tutorial da linguagem Lua
  e também da sua API C, levando em consideração as diferenças entre
  versões;
* [**Bibliografia**](documentos/bibliografia/index.md): apresentação de
  materiais bibliográficos sobre Lua, com comentários;
* [**Glossário**](documentos/glossario.md): um glossário de termos usuais
  na terminologia de Lua.
* [**Cronologia**](documentos/cronologia.md): enumeração de eventos
  relevantes para o escopo do Lu-a-Bá;
* [**Versões**](documentos/versoes/index.md): comentários sobre as
  principais versões lançadas e alterações entre elas;
* [**Comparativos**](documentos/comparativos/index.md): comparativos com
  linguagens que influenciaram ou possuem alguma importância para Lua;

Para saber mais sobre o projeto vide [este documento](documentos/sobre.md) e
também consulte as [convenções do projeto](convencoes.md).

## Licença

Exceto onde for informado ao contrário, o conteúdo deste repositório
está sob a licença [Atribuição-CompartilhaIgual 4.0 Internacional
(CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)

Vide [texto na íntegra](LICENSE).
