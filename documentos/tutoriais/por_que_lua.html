[% META
   titulo = "Por que Lua?"
%]
[% PROCESS geral.tt %]
[% WRAPPER artigo.tt %]
<p>Antes de começar a estudar Lua, é natural que você se pergunte por que
motivo faria isso. Algumas vezes, o motivo é dado por força maior, ou seja,
um problema envolvendo Lua aparece, e você se vê na obrigação de aprender.
Outras vezes pode partir de uma curiosidade ou especulação sobre precisar
disso no futuro.</p>

<p>Nos dois casos, você vai querer antes entender o que Lua tem de bom a
oferecer, para justificar o tempo que investirá nesse estudo (ainda que Lua
seja uma linguagem bastante acessível, até mesmo para quem possui pouca ou
nenhuma experiência com programação).</p>

<p>Mas preciso fazer um alerta aos incautos: aprender a programar e aprender
uma linguagem de programação (seja Lua ou qualquer outra), por mais que sejam
tarefas relacionadas, não são a mesma coisa.</p>

<p>Inclusive, é por isso que é possível aprender Lua tendo uma base de
conhecimento em programação pouco consolidada. Por outro lado, você vai tirar
mais proveito da linguagem na medida que entender que tipo de problemas quer
resolver, e de que maneiras pode resolvê-los com eficiência.</p>

<p>Dito isso, por que você poderia se beneficiar desse estudo? Não é possível
prever todas as situações em que esse conhecimento vai ser benéfico, mas se
você compreender as principais características de Lua, então pode pelo menos
prever algumas dessas situações.</p>

<h2 id="caractersticas-de-lua">Características de Lua</h2>

<p>Na página oficial de Lua em português, consulte a seção <a href=
"https://lua.org/portugues.html#porque">Por que escolher Lua?</a> para saber
mais sobre os principais motivos que poderiam pesar em favor dessa
escolha.</p>

<p>Além do exposto nessa seção, podemos destacar as seguintes
características:</p>

<ul>
  <li>Linguagem de alto nível;</li>

  <li>Ambiente interpretado;</li>

  <li>Coleta de lixo;</li>

  <li>Metaprogramação;</li>

  <li>Multi-paradigma;</li>

  <li>Tipagem dinâmica;</li>

  <li>Apenas 8 tipos de dados (e derivações);</li>

  <li>Funções como valores de primeira classe com escopo léxico;</li>

  <li>Chamadas finais próprias;</li>

  <li>Tabelas (vetores associativos) metaprogramáveis;</li>

  <li>Co-rotinas;</li>

  <li>Suporte a módulos;</li>

  <li>API C;</li>

  <li>Inteiramente escrito em C89.</li>
</ul>

<p>Não se preocupe se não entender o que todas essas características
significam, veremos tudo isso mais adiante, ao longo dos tutoriais.</p>

<h2 id="economia-de-conceitos">Economia de conceitos</h2>

<p>A linguagem Lua é marcada pela sua simplicidade. Existem poucos conceitos
nela com os quais você precisa se familiarizar. Nesse sentido, é uma
linguagem pequena e fácil de guardar na sua memória (não digo a do
computador, mas a sua mesmo).</p>

<p>Esses poucos conceitos podem ser combinados e recombinados como peças de
montar, que você usa para construir objetos maiores. Isto não é uma abordagem
muito comum, então pode levar algum tempo para se acostumar, mas logo você
verá que isso não é restritivo. Pelo contrário, amplia as possibilidades.</p>

<p>Essa economia de conceitos é, aliás, o cerne da
<strong>metaprogramação</strong> em Lua. Os três pilares básicos da linguagem
são as funções, as tabelas e as co-rotinas. Quando combinados, eles podem ser
metaprogramados para assumir comportamentos mais específicos, alinhados à
necessidade do programa.</p>

<p>A partir deles, podemos usar paradigmas diferentes em Lua (procedural,
funcional, orientado a objetos), criar ambientes segregados para execução,
criar e importar módulos e realizar o tratamento de erros.</p>

<p>A base de tudo isso em Lua são esses três conceitos. A partir deles, você
pode definir as regras do seu ambiente de execução, no lugar de usar regras
pré-determinadas, como ocorre na maior parte dos ambientes de
desenvolvimento.</p>

<p>Como bônus, essa economia torna muito mais fácil memorizar como as coisas
funcionam em Lua.</p>

<h2 id="integrao-com-outros-ambientes">Integração com outros ambientes</h2>

<p>Um dos pontos fortes de Lua é a possibilidade de integração de toda a sua
funcionalidade com outras aplicações, usando a API C.</p>

[% WRAPPER nota.tt titulo="O que é API?" %]
<p>API é uma abreviação para o termo <em>Application Programming
Interface</em> (do inglês, traduzido como interface de programação de
aplicações), e serve para expor a funcionalidade de uma aplicação em
algum ambiente de desenvolvimento específico, sem que seja necessário
conhecer o seu funcionamento interno.</p>
[% END %]

<p>Você pode escrever alguns programas inteiramente em Lua, mas também é
bastante comum combinar Lua com outros ambientes de desenvolvimento. Em
muitos casos, Lua é um ambiente secundário, funcionando como um motor de
extensões&#47;configurações <strong>embutido</strong> em outra aplicação.</p>

<p>Não raro, um programa precisa de um mecanismo específico para ler seus
arquivos de configuração, definir uma interface de depuração, possuir algum
método de extensão (para criação de módulos do usuário, ou <em>plugins</em>,
por exemplo), ou ainda possuir várias funcionalidades que já são
implementadas pela biblioteca Lua.</p>

<p>Neste caso, em vez de reescrever toda essa funcionalidade, a biblioteca
Lua pode ser embutida no programa, o que facilita esse trabalho. Por ser uma
biblioteca bem pequena e enxuta, o custo desse acoplamento acaba por ser
baixo em proporção ao ganho.</p>

<p>Outro ponto interessante sobre isto é o fato de que Lua é um ambiente
escrito inteiramente em C89, que é comum a C e C++, portanto você pode
compilar a biblioteca Lua do mesmo modo usando um compilador C ou C++.</p>

<p>Em muitos casos, quando você vier a programar em Lua, pode ser que o
<strong>programa hospedeiro</strong> já exista, e a sua tarefa será criar os
módulos ou configurações em Lua que serão usados pelo programa.</p>

<p>O próprio interpretador <code>lua</code> é um exemplar de programa
hospedeiro (e ele será nossa principal ferramenta ao longo dos tutoriais).
Ele cuida de algumas tarefas básicas como controlar a entrada e a saída do
programa e tratar erros, por exemplo, mas a maior parte do trabalho é
realizada pela biblioteca Lua embutida nele.</p>

<p>Além de poder ser embutida, a biblioteca Lua pode ser também
<strong>estendida</strong>, com módulos externos, escritos em outras
linguagens. Por meio da API C, você pode criar funções, tipos e módulos, e
expor em Lua.</p>

<p>Isso é bastante útil se levarmos em conta que Lua possui uma biblioteca
padrão bastante enxuta. Então, para adicionar funcionalidades que não são
cobertas por ela, podemos lançar mão de módulos externos.</p>

<p>Também é útil como forma de integração entre aplicações. Digamos que você
está trabalhando em um projeto que envolve aplicações diferentes, cada qual
possui uma API própria, e você precisa expor a funcionalidade das duas em uma
linguagem de alto nível.</p>

<p>Por meio da API C de Lua, você pode conectar essas aplicações em uma mesma
interface, na qual cada aplicação externa é carregada como um módulo
adicional. A partir desse ponto, elas podem ser acessadas em um mesmo
programa em Lua.</p>

<p>Existem também situações na qual alguma operação é altamente custosa para
executar em um ambiente interpretado. Nesse caso, você pode desenvolver essas
partes em C ou C++, por exemplo, e em Lua apenas expor mecanismos de acesso a
essas funcionalidades pré-compiladas, conseguindo um ganho no desempenho.</p>

<h2 id="segmentos-de-destaque">Segmentos de destaque</h2>

<p>A essa altura você já deve estar imaginando em que situações Lua é um
ambiente de destaque hoje. Como é de se esperar, existem alguns espaços onde
o uso de Lua é mais favorável do que em outros, pelas características que
possui.</p>

<ul>
  <li>
    <p><strong>Jogos</strong>: Seria até estranho não começar por aqui. Lua
    ganhou uma boa reputação no segmento de jogos, e não foi por acaso.
    Primeiramente, por ser uma linguagem flexível e expressiva, é útil para
    roteiristas e artistas conseguirem programar o funcionamento de um jogo,
    ainda que não sejam programadores profissionais.</p>

    <p>Segundo, porque sendo pequena e integrável, pode ser embutida em um
    motor de jogos com funções de baixo nível ou mais complexas, que são
    abstraídas por uma interface de alto nível, em Lua, permitindo um foco
    maior na lógica do jogo.</p>

    <p>E ainda, dispõe de mecanismos flexíveis, como co-rotinas, que ajudam a
    controlar o comportamento de vários objetos em uma lógica multi-tarefa,
    onde pode se trabalhar a noção de ações simultâneas entre esses
    objetos.</p>
  </li>

  <li>
    <p><strong>Embarcados</strong>: Este é outro segmento de grande destaque,
    pois Lua possui apenas a biblioteca C como dependência, além de ser muito
    enxuta, portanto se torna viável em dispositivos com capacidade limitada
    (com pouca memória e espaço de armazenamento).</p>

    <p>Lua também possui uma API que expõe toda a sua funcionalidade,
    portanto todo o potencial da linguagem pode ser explorado, mesmo em
    dispositivos mais limitados.</p>

    <p>Isto faz com que Lua seja um forte candidato para integração de
    funcionalidades em televisores, painéis de carros, sensores climáticos,
    máquinas de café, terminais de pagamento e todo tipo de dispositivo
    programável.</p>
  </li>

  <li>
    <p><strong>Segurança</strong>: Ainda outra área de conhecimento que tem
    se beneficiado de Lua é a cibersegurança. Nesse segmento, é necessário
    trabalhar com sistemas flexíveis e que permitam uma resposta rápida,
    tanto a ataques&#47;incidentes como a mecanismos de defesa (do ponto de
    vista de um teste de intrusão, por exemplo).</p>

    <p>A flexibilidade e portabilidade são úteis para que esse ambiente seja
    integrado a dispositivos e programas de teste de intrusão, assim como
    também é útil em dispositivos de rede, filtros de pacotes, ferramentas de
    detecção de intrusão, de análise de tráfego, balanceadores de carga e
    sistemas de teste e inspeção de malwares.</p>
  </li>

  <li>
    <p><strong>Multimídia</strong>: Ferramentas de multimídia quase sempre
    dispõem de sistemas de extensões e preferências que permitem a criação ou
    modulação de efeitos visuais, definição de atalhos e criação de funções
    que alteram o comportamento do programa.</p>

    <p>Nesse sentido, Lua é um candidato natural, tanto pela portabilidade e
    tamanho da biblioteca como pela expressividade e flexibilidade da
    linguagem.</p>
  </li>

  <li>
    <p><strong>Processamento de texto</strong>: Em geral, ferramentas de
    processamento de texto utilizam modelos e funções de transformação, que
    atuam sobre o texto informado produzindo a saída desejada.</p>

    <p>Lua é uma excelente maneira de descrever dados, portanto útil para
    definição dos modelos de entrada. Também é útil para definir as funções
    de leitura de textos de entrada e produzir saídas personalizadas.</p>
  </li>
</ul>

<p>E não entenda como se essa lista fosse exaustiva. Qualquer situação na
qual você poderia se beneficiar de uma biblioteca acoplada com baixo custo
computacional, seja para facilitar a configuração ou para prover extensões ou
uma interface programável é propícia para se considerar o uso de Lua.</p>

<p>Igualmente, situações na qual você precisa integrar aplicações diferentes
provendo uma interface de alto nível também são cenários favoráveis ao uso da
linguagem.</p>

<h2 id="gostei-e-agora">Gostei! E agora?</h2>

<p>Se interessou? Então agora é o momento de <a href="obtendo_lua.html">obter a
biblioteca Lua</a>, ou se já tiver feito isso, aprender o básico sobre a
<a href="executando_codigo_lua.html">execução de código Lua</a>.</p>

<h2 id="contedo-relacionado">Conteúdo relacionado</h2>

<ul>
  <li>Bibliografia &#8211; <a href="../bibliografia/wjogos04.html">A
  Linguagem Lua e suas Aplicações em Jogos</a>
  </li>

  <li>Bibliografia &#8211; <a href=
  "../bibliografia/look_design_lua.html"><em>A Look at the Design of
  Lua</em></a>
  </li>

  <li>Bibliografia &#8211; <a href="../bibliografia/hopl3.html"><em>The
  Evolution of Lua</em></a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#api-c">API C</a>
  </li>

  <li>Glossário &#8211; <a href=
  "../glossario.html#biblioteca-padro">Biblioteca padrão</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#chamada-final">Chamada
  final</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#co-rotina">Co-rotina</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#coletor-de-lixo">Coletor
  de lixo</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#depurao">Depuração</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#escopo-lxico">Escopo
  Léxico</a>
  </li>

  <li>Glossário &#8211; <a href=
  "../glossario.html#interpretador-lua">Interpretador Lua</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#mdulo">Módulo</a>
  </li>

  <li>Glossário &#8211; <a href=
  "../glossario.html#programa-hospedeiro">Programa hospedeiro</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#tabela">Tabela</a>
  </li>

  <li>Glossário &#8211; <a href=
  "../glossario.html#valores-de-primeira-classe">Valores de primeira
  classe</a>
  </li>
</ul>
[% END %]
