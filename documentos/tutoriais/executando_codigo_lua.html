[% META
   titulo = "Executando código Lua"
%]
[% PROCESS geral.tt %]
[% WRAPPER artigo.tt %]
<p>
Existem várias maneiras de executar código Lua. Neste tutorial, veremos
algumas delas.
</p>

<p>
Nossa principal ferramenta será o interpretador <code>lua</code>, que
usamos para executar código Lua. Ele pode receber o código por vários métodos
diferentes, e então se encarrega da execução.
</p>

<p>
Por enquanto, vamos trabalhar com três métodos: interativo, por arquivos e
por argumentos de linha de comando.
</p>

<p>
Também vamos explorar outra ferramenta, o compilador <code>luac</code>,
que podemos usar para agilizar o carregamento de códigos Lua e verificar por
erros eventuais.
</p>

<p>
Se você ainda não <a href="obtendo_lua.html">obteve Lua</a>, deverá cumprir
este requisito primeiro. Feito isso, terá os programas <code>lua</code> e
<code>luac</code>, que usaremos aqui.
</p>

<h2 id="modo-interativo">Modo interativo</h2>

<p>
Lua é uma linguagem dinâmica. Podemos informar um comando ao interpretador
e já observar os seus resultados, antes de passar para a execução do próximo
comando. Isto facilita o aprendizado. Vamos começar explorando essa
característica.
</p>

<p>
Como vimos no tutorial anterior, ao executar o programa <code>lua</code>
sem nenhum parâmetro, veremos o seu <em>prompt</em> de comandos.
</p>

[% WRAPPER codigo.tt %]
$ lua
[% PROCESS luacab.tt %]
> 
[% END %]

<p>
Recapitulando, o sinal <code>&gt;</code> (isto é, o <em>prompt</em>) quer
dizer que o interpretador está à espera de um próximo comando. Ao inserir o
comando (seguido da tecla <code>Enter</code>), o interpretador vai executar o
comando informado, que pode ou não ter alguma saída na tela (ou seja, algum
efeito visível).
</p>

<p>
Vamos começar com um comando bem simples, que apenas exibirá na tela do
seu terminal o texto &#8220;Estou na Lua&#8221;.
</p>

[% WRAPPER codigo.tt %]
> print("Estou na Lua")
Estou na Lua
[% END %]

<p>
Neste comando estamos chamando a função <code>print</code> que serve para
exibir algo na tela. Informamos o valor que quermos exibir, e a função print
escreve (ou <strong>imprime</strong>, na terminologia original) na tela.
</p>

<p>
Essa não é a única forma de fazer algo aparecer na tela. Existe outro
método, que é <strong>retornar</strong> algum valor. Um valor como o texto
&#8220;Estou na Lua&#8221; que usamos antes, por exemplo.
</p>

[% WRAPPER codigo.tt %]
> return "Estou na Lua"
Estou na Lua
[% END %]

<p>
Parece confuso que haja duas maneiras de obter o mesmo efeito? Pode ser,
mas logo veremos a diferença entre esses dois métodos. Por enquanto, vamos
nos ater a um fato mais pragmático: podemos omitir o comando
<code>return</code>:
</p>

[% WRAPPER codigo.tt %]
> "Estou na Lua"
Estou na Lua
[% END %]

<p>
Há uma observação importante a fazer, sobre essa omissão do comando
<code>return</code>: até Lua 5.2, era necessário inserir um sinal de igual
antes do valor retornado (o que ainda funciona nas versões mais novas):
</p>

[% WRAPPER codigo.tt %]
> = "Estou na Lua"
Estou na Lua
[% END %]

<p>
A partir da versão 5.3, isso deixou de ser necessário, e podemos omitir
também o sinal de igual.
</p>

<p>
Até aqui trabalhamos apenas com um valor, que é um texto. A rigor, ele é o
que chamamos de uma <strong>expressão</strong>. Em Lua, existem vários tipos
de expressão, e o texto é um deles.
</p>

<p>
Mas além do texto, números são expressões, assim como operações
aritméticas sobre números. Vejamos:
</p>

[% WRAPPER codigo.tt %]
> 3
3
> 2 + 2
4
[% END %]

<p>
O exemplo de operação aritmética nos mostra que o interpretador pode ser
usado como uma calculadora. Mas ele vai muito além disso. Podemos trabalhar
com variáveis também:
</p>

[% WRAPPER codigo.tt %]
> i = 3
[% END %]

<p>
Note que esse comando não emite nenhuma saída. Note também que eu disse
<em>comando</em> e não <em>expressão</em>. Tecnicamente, há uma diferença.
Expressões sempre são retornadas, e portanto você sempre verá uma saída para
elas. Comandos não são retornados, e eles podem emitir alguma saída, ou
não.
</p>

<p>
Neste caso, estamos executando um comando de <strong>atribuição</strong>
de um valor (<code>3</code>) a uma variável (<code>i</code>). Dito isso,
podemos criar expressões mais complexas e até mesmo utilizar variáveis dentro
delas.
</p>

[% WRAPPER codigo.tt %]
> i = 3
> 2 * (i + 14) + i * i
43
[% END %]

<p>
Ou seja, na primeira linha, informamos um comando (de atribuição), e na
segunda linha informamos uma expressão. Relembrando, ao informar uma
expressão ao interpretador, ele retorna seu resultado.
</p>

<p>
Para ser ainda mais preciso, quando visualizamos o <em>prompt</em>, isso
significa que o interpretador está à espera do que chamamos, na terminologia
de Lua, de um <strong>trecho</strong>.
</p>

<p>
Um trecho é um agrupamento lógico (um bloco), que é interpretado de uma
vez só, e pode conter um ou mais comandos ou blocos internos.
</p>

<p>
Então podemos em uma mesma linha informar mais de um comando.
</p>

[% WRAPPER codigo.tt %]
> i = 3 print(i)
3
> i = 2 return i
2
[% END %]

<p>
Veja que não foi necessário nem mesmo separar a atribuição de
<code>i</code> da chamada ao comando posterior. Podemos, por uma questão
puramente estética, inserir um ponto-e-vírgula entre os dois comandos. Isso
não afeta em nada a saída.
</p>

[% WRAPPER codigo.tt %]
> i = 3 ; print(i)
3
[% END %]

<p>
E por falar em estética, outro recurso que ajuda a melhorar a visualização
do código ao usar o interpretador, é dividir partes de um mesmo trecho em
mais de uma linha. Quando está óbvio para o interpretador que o trecho
informado está incompleto, ele aguardará pelo resto antes de executar.
</p>

<p>
Vejamos um exemplo:
</p>

[% WRAPPER codigo.tt %]
> print (
>> 2 +
>> 2
>> )
4
[% END %]

<p>
Veja que ao chamar a função <code>print</code>, abrimos parênteses, mas
não fechamos na mesma linha, dando a entender que isso será feito em outra
linha. O interpretador então altera o <em>prompt</em> para dois sinais de
maior (<code>&gt;&gt;</code>) no lugar de apenas um, indicando que aguarda
pelo resto do trecho, antes de executá-lo.
</p>

<p>
Antes de encerrar a chamada de função informamos uma expressão (<code>2 +
2</code>), que por acaso também foi quebrada em mais de uma linha. Somente ao
informar o fechamento dos parênteses o trecho é dado como completo e o
interpretador o executa.
</p>

<p>
Esse <em>prompt</em> modificado que vimos, chamamos usualmente de
<em>prompt secundário</em>. No exemplo acima, pode não fazer tanto sentido
dividir o trecho, por ser demasiado simples, mas em trechos envolvendo
definições de funções ou laços repetitivos (que veremos em outros tutoriais)
o uso desse recurso será mais produtivo.
</p>

<p>
Para encerrar o modo interativo, você pode usar, como vimos no tutorial
anterior, uma chamada à função <code>os.exit()</code> (ainda exploraremos
funções em outro tutorial).
</p>

[% WRAPPER codigo.tt %]
> os.exit()
[% END %]

<p>
Um modo ainda mais prático é inserir um caractere de fim de linha, embora
isso seja específico do sistema operacional. Tipicamente, em sistemas POSIX,
podemos inserí-lo com a combinação <code>Ctrl+D</code>, e no terminal do
Windows com a combinação <code>Ctrl+Z</code>.
</p>

<p>
Bem, até aqui acho que já deu para entender o básico de como funciona o
interpretador no modo interativo.
</p>

<p>
E a propósito, não subestime esse método de desenvolvimento. Ele não
apenas é uma ferramenta de grande valor para o aprendizado, como também
permite explorar o seu programa enquanto o desenvolve. E ele pode ainda ser
enriquecido por outros métodos, como o uso de arquivos-fonte Lua, como
veremos a seguir.
</p>

<h2 id="arquivos-fonte-lua">Arquivos-fonte Lua</h2>

<p>
Do mesmo modo que podemos informar trechos interativamente ao
interpretador <code>lua</code>, podemos também armazenar todo o seu conteúdo
em um arquivo, e então passar este arquivo como argumento para o
interpretador.
</p>

<p>
Convencionalmente chamamos esses arquivos de
<strong>arquivo-fonte</strong>, visto que ele contém código-fonte
Lua, que será executado pelo interpretador. Um arquivo-fonte não
possui nada de muito especial, é um arquivo de texto comum, no qual
inserimos os trechos Lua que queremos executar. Também é comum
chamar esses arquivos de <em>scripts</em>.
</p>

[% WRAPPER nota.tt titulo = "O que são scripts?" %]
<p>
É comum usar o termo <em>script</em> (do inglês, roteiro), em
referência a programas que funcionam como um passo a passo composto por
funções mais primitivas, que sabemos para que servem, mas abstraímos seu
funcionamento interno.
</p>
[% END %]

<p>
Também, por convenção, utilizamos a extensão <code>.lua</code> nesses
arquivos, para denotar que se trata de um arquivo-fonte Lua. Você pode
utilizar qualquer editor de texto simples como o editor <code>vi</code> em
sistemas POSIX ou o bloco de notas do Windows, por exemplo.
</p>

<p>
Talvez eles não ofereçam muito conforto para programar, mas é fato que
eles servem a esse propósito, na falta de uma opção mais ergonômica.
</p>

[% WRAPPER nota.tt titulo = "Em defesa do vi" %]
<p>
Para ser justo com o <code>vi</code>, ele é um editor bastante
capaz, para a programação também, embora menos intuitivo. Exploro esse
assunto no texto <a href="/blog/2024/fev/simples_casa_simples.html">Simples
casa com simples</a>, no blog.
</p>
[% END %]

<p>
Alguns editores de texto, como <a href="https://www.vim.org">vim</a> ou
<a href="https://www.geany.org">geany</a> por exemplo, já oferecem mais
recursos para a programação, e talvez sejam mais ao seu estilo.
</p>

<p>
Seja como for, Lua é uma linguagem simples, o que elimina a necessidade de
um editor muito avançado para programar. Essa é uma das vantagens de ter a
simplicidade como uma característica impressa na linguagem.
</p>

<p>
Então vamos começar com um teste básico. Crie um arquivo chamado
<code>astronauta.lua</code> com o conteúdo:
</p>

[% WRAPPER codigo.tt tipo = "lua" %]
print("Estou na Lua")
[% END %]

<p>
Depois, execute no terminal o seguinte comando:
</p>

[% WRAPPER codigo.tt %]
$ lua astronauta.lua
Estou na Lua
[% END %]

<p>
O resultado, como podemos ver, será a exibição na tela do texto
&#8220;Estou na Lua&#8221;, igual já vimos antes na execução interativa. É
importante notar que como não estamos no modo interativo, precisamos recorrer
à função <code>print</code> para exibir uma saída.
</p>

<p>
Perceba também que nesse texto de exemplo não temos nenhuma ocorrência de
acentos ou cedilha. Isso não foi por acaso. Arquivos-fonte são, como falei,
arquivos de texto, e portanto estão sujeitos a questões relacionadas à
codificação de texto, algo que não vamos explorar agora.
</p>

[% WRAPPER nota.tt titulo = "O que é codificação de caracteres?" %]
<p>
Codificação de caracteres é o modo como o texto é traduzido para bits
(portanto legíveis pela máquina), e vice-versa.
</p>

<p>
Este assunto será melhor explorado adiante, em &#8220;Codificação de
caracteres&#8221;, na seção <a href="#adendos">adendos</a>.
</p>
[% END %]

<p>
Por hora, apenas saiba que existem situações em que, executar no terminal
um arquivo-fonte contendo acentos e cedilhas pode emitir alguns caracteres
estranhos na tela, então por enquanto vamos evitá-los.
</p>

<p>
Voltando ao tema central deste tópico, a execução de códigos a partir de
arquivo-fonte possui como vantagem o fato de que podemos armazenar nosso
código em um arquivo e reexecutá-lo tantas vezes quanto for preciso, e
gradualmente fazer modificações no programa, quando necessário.
</p>

<p>
Para ver como isso é prático faça uma modificação no arquivo
<code>astronauta.lua</code>, insira nele uma segunda linha, com o
comando:
</p>

[% WRAPPER codigo.tt tipo = "lua" %]
print("Sinto falta da Terra")
[% END %]

<p>
Agora execute novamente, e você verá não uma, mas duas mensagens, como a
seguir:
</p>

[% WRAPPER codigo.tt %]
$ lua astronauta.lua
Estou na Lua
Sinto falta da Terra
[% END %]

<p>
O ponto a se observar aqui é que você não precisou reescrever o primeiro
comando, apenas inserir o segundo. À medida que criar programas maiores,
perceberá o quanto é conveniente salvar em arquivos-fonte as partes que quer
reaproveitar (ou que pode querer modificar no futuro).
</p>

<h3 id="comentrios">Comentários</h3>

<p>
Outra vantagem no uso de arquivos-fonte é o fato de que podemos inserir
neles alguns comentários. Como em várias outras linguagens, comentários não
afetam a execução do programa, mas podem ser úteis para o programador, ao ler
o código do programa.
</p>

<p>
Comentários servem para facilitar a compreensão do programa, em geral para
alertar sobre algo que não deve ser modificado, ou para explicar por que algo
foi feito de determinado modo.
</p>

<p>
Em Lua, comentários são iniciados pela sequência de caracteres
<code>--</code> (dois hífens), e se estendem até o fim da linha. Vamos
adicionar um comentário ao programa <code>astronauta.lua</code>, de modo que
ele possua o seguinte conteúdo:
</p>

[% WRAPPER codigo.tt tipo = "lua" %]
-- Mensagens de um astronauta

print("Estou na Lua")
print("Sinto falta da Terra")
[% END %]

<p>
Veja que na primeira linha inserimos um comentário. Execute o programa, e
perceba que o comentário em nada alterou o comportamento do programa, mas ao
ler o arquivo <code>astronauta.lua</code> você verá a explicação de que o
programa emite mensagens de um astronauta.
</p>

<p>
Comentários podem ser inseridos na mesma linha onde há um comando (ou
mesmo parte de um comando). Muitas vezes, fazemos isso para anunciar qual é o
resultado que se espera da execução de uma linha específica. Por exemplo:
</p>

[% WRAPPER codigo.tt tipo = "lua" %]
print(2+2) --> 4
[% END %]

<p>
O sinal de maior (<code>&gt;</code>) logo após o início do comentário não
muda absolutamente nada, apenas é uma convenção utilizada quando a finalidade
é justamente essa de anunciar um resultado esperado, pois o formato de seta
pode ter essa conotação de &#8220;resultado&#8221;.
</p>

<p>
Além desse comentário de linha, existem também os comentários de blocos,
eles abrangem um grupo de linhas entre os seus delimitadores. Vejamos um
exemplo.
</p>

[% WRAPPER codigo.tt tipo = "lua" %]
--[[
    Astronauta
    ----------
    
    Mensagens de um astronauta na Lua.
--]]

print("Estou na Lua")
print("Sinto falta da Terra")
[% END %]

<p>
Veja que usamos os delimitadores <code>--[[</code> e <code>--]]</code>
para, respectivamente, abrir e fechar o bloco, que vai da primeira à sexta
linha desse exemplo.
</p>

<p>
Comentários de bloco são úteis para mensagens mais longas, que necessitem
várias linhas, mas não apenas isso. Também são úteis em situações nas quais
você pode querer facilmente &#8220;ativar&#47;desativar&#8221; determinado
trecho de código.
</p>

<p>
Vejamos um exemplo:
</p>

[% WRAPPER codigo.tt tipo = "lua" %]
i = 3
--[[
print("ALERTA: usando valor dobrado")
i = 2 * i
--]]
print(i)
[% END %]

<p>
Neste exemplo, atribuímos um valor à variável <code>i</code>, e depois
imprimimos esse valor na tela. Porém no meio há um comentário de bloco. Sendo
um comentário, não afeta o resultado da execução.
</p>

<p>
Mas digamos que eu queira testar o resultado da execução fazendo com que o
código dentro do comentário passe a ser válido. Muito simples: adicionamos um
hífen extra no delimitador de abertura do bloco, do seguinte modo:
</p>

[% WRAPPER codigo.tt tipo = "lua" %]
i = 3
---[[
print("ALERTA: usando valor dobrado")
i = 2 * i
--]]
print(i)
[% END %]

<p>
Notou a diferença na abertura do comentário? Agora são três hífens no
lugar de dois. Isso faz com que ele passe a ser um comentário normal, e não
mais uma abertura de comentário de bloco (e o de fechamento também passa a
ser tratado como um comentário normal).
</p>

<p>
Logo, todo o código dentro do bloco passa a ser válido. Para desativar
novamente, basta remover esse hífen extra.
</p>

<h3 id="chamando-arquivo-em-cdigo-lua">Chamando arquivo em código Lua</h3>

<p>
Lembra quando eu disse que podemos combinar o método interativo com o uso
de arquivos-fonte? Chegou a hora de ver como. Em Lua, existe uma função
chamada <code>dofile</code>, que lê o conteúdo de um arquivo-fonte e o
executa.
</p>

<p>
Podemos chamar essa função enquanto executamos interativamente o
interpretador. Vejamos um exemplo com nosso script,
<code>astronauta.lua</code>.
</p>

[% WRAPPER codigo.tt %]
> dofile("astronauta.lua")
Estou na Lua
Sinto falta da Terra
[% END %]

<p>
Nosso arquivo <code>astronauta.lua</code> apenas emite algumas mensagens,
portanto se executarmos de novo, o resultado será exatamente o mesmo, ou
seja, as mensagens serão exibidas de novo. Em outros casos, poderíamos ter um
arquivo-fonte cuja execução depende do valor de alguma variável, e nesse caso
o resultado poderá ser diferente a cada nova chamada.
</p>

<p>
Podemos usar este recurso para realizar uma sequência de cálculos e
validações, e ao final retornar o resultado. Vamos ver um outro exemplo de
arquivo-fonte, que realiza uma operação aritmética e retorna seu
resultado.
</p>

[% WRAPPER codigo.tt tipo = "lua" %]
return 2 * (i + 14) + i * i
[% END %]

<p>
Salve este arquivo como <code>calculo.lua</code>, e em seguida, execute
interativamente, definindo previamente o valor de <code>i</code>:
</p>

[% WRAPPER codigo.tt %]
> i = 3
> dofile("calculo.lua")
43
[% END %]

<p>
Veja que o arquivo utilizado possui um retorno, que corresponde ao
resultado da expressão. Neste caso, poderíamos definir qualquer valor para
<code>i</code> e reexecutar o cálculo, obtendo um novo resultado. Esse
retorno é apresentado na tela, como qualquer outro retorno seria (o que já
vimos na seção anterior).
</p>

<p>
Se quisermos, podemos salvar este retorno em uma variável, no lugar de
exibir na tela:
</p>

[% WRAPPER codigo.tt %]
> i = 3
> resultado = dofile("calculo.lua")
> resultado
43
[% END %]

<p>
A função <code>dofile</code> não precisa ser chamada apenas no modo
interativo. Podemos usá-la dentro de arquivos-fonte também, de modo que
teremos assim chamadas aninhadas, um arquivo chamando outro arquivo. Porém,
em tutoriais futuros veremos maneiras mais interessantes de realizar esse
tipo de operação.
</p>

<h3 id="pr-executar-arquivo">Pré-executar arquivo</h3>

<p>
Além da função <code>dofile</code>, temos ainda outra possibilidade de
combinação dos modos de execução. Em vez de chamar o arquivo após iniciado o
modo interativo, podemos informar um parâmetro (<code>-i</code>) para o
interpretador <code>lua</code>, seguido do nome de um ou mais arquivos-fonte
Lua que queremos pré-executar.
</p>

<p>
Isto fará com que o arquivo seja executado <em>antes</em> de entrar no
modo interativo. Vejamos um exemplo com o nosso já conhecido
<code>astronauta.lua</code>:
</p>

[% WRAPPER codigo.tt %]
$ lua -i astronauta.lua
[% PROCESS luacab.tt %]
Estou na Lua
Sinto falta da Terra
> 
[% END %]

<p>
Perceba a ordem dos eventos neste caso: primeiro o interpretador Lua é
iniciado (a mensagem de direitos autorais que aparece no início está aí para
nos mostrar isso), depois nosso arquivo <code>astronauta.lua</code> é
executado, exibindo as mensagens de saída, e por fim vemos o <em>prompt</em>
de comandos do interpretador.
</p>

<p>
Neste caso, usamos um arquivo simples, que apenas emite mensagens. Mas
poderíamos ter usado um arquivo que define variáveis ou novas funções, o que
nos habilita a criar um ambiente que pode ser reutilizado toda vez que
iniciarmos o interpretador.
</p>

<p>
A título de exemplo, crie um arquivo chamado <code>ambiente.lua</code>,
com o seguinte conteúdo:
</p>

[% WRAPPER codigo.tt tipo = "lua" %]
i = 3
function dobro(n) return n * 2 end
[% END %]

<p>
Neste exemplo definimos uma variável e uma função (veremos mais sobre
criação de funções em outro tutorial), que vamos utilizar no modo interativo.
Então inicie o modo interativo importando esse arquivo, como a seguir:
</p>

[% WRAPPER codigo.tt %]
$ lua -i ambiente.lua
> i
3
> dobro(i)
6
[% END %] 

<p>
Isto ficará ainda mais interessante quando explorarmos o conceito de
módulos, mas por enquanto acho que você já possui algumas ideias para colocar
em prática. Vamos explorar módulos em outro momento.
</p>

<h2 id="na-linha-de-comando">Na linha de comando</h2>

<p>
Além de informar código Lua em um interpretador interativo e via
arquivos-fonte, podemos ainda informar o código como parâmetro de linha de
comando para o interpretador <code>lua</code>, isto é, de modo não
interativo.
</p>

<p>
Veja um exemplo:
</p>

[% WRAPPER codigo.tt %]
$ lua -e 'print("Amigo, estou aqui.")'
Amigo, estou aqui.
[% END %]

<p>
O que acontece neste exemplo é que o interpretador <code>lua</code> é
iniciado, recebe um pequeno trecho de código como argumento e o executa. Uma
vez executado esse trecho, o interpretador é encerrado.
</p>

<p>
Não parece muito útil, não é mesmo? Bem, é útil para alguns pequenos
testes, mas podemos incrementar essa execução não interativa, ao encadear o
parâmetro <code>-e</code> diversas vezes.
</p>

[% WRAPPER codigo.tt %]
$ lua -e 'i = 2 + 2' -e 'print(i)'
4
[% END %]

<p>
Note que no primeiro trecho temos uma atribuição da variável
<code>i</code> e no segundo trecho passamos <code>i</code> como argumento
para a função <code>print</code>. Isso mostra que a ordem dos trechos
informados importa, e que variáveis definidas em um trecho anterior podem ser
usadas em um trecho posterior.
</p>

<p>
Podemos combinar esse modo de execução com arquivos-fonte, ao executar a
função <code>dofile</code> em um dos trechos informados, veja:
</p>

[% WRAPPER codigo.tt %]
$ lua -e 'i = 3' -e 'print(dofile("calculo.lua"))'
43
[% END %]

<p>
Existe diferença entre usar vários parâmetros <code>-e</code> separados ou
usar um só com todo o código? Não muita, mas existe.
</p>

<p>
Além da diferença estética óbvia, o que permite reorganizar a chamada de
linha de comando para ficar mais legível, cada parâmetro <code>-e</code>
informado é um trecho Lua por si só.
</p>

<p>
E vamos lembrar, cada trecho de código é &#8220;compilado&#8221;
individualmente (veremos na seção seguinte o que essa compilação significa),
o que permite que você determine explicitamente a ordem de compilação dos
trechos, como se os estivesse informando, um por um, no modo interativo.
</p>

<p>
E antes que me esqueça de dizer, podemos também combinar esse modo de
execução (na linha de comando) com o uso de arquivo-fontes sem recorrer à
função <code>dofile</code>, informando o arquivo após todas as opções do
interpretador. Por exemplo:
</p>

[% WRAPPER codigo.tt %]
$ lua -e 'print("Mensagens de um astronauta:")' astronauta.lua
[% END %]

<p>
Veja que nesse caso executamos o trecho da linha de comando primeiro, e em
seguida o arquivo-fonte. Isso pode ser útil na realizaçao de testes rápidos
com arquivos-fonte que precisem de comandos pré-executados.
</p>

<h2 id="o-compilador-lua">O compilador Lua</h2>

<p>
Falar em <strong>compilação em Lua</strong> pode confundir algumas
pessoas, ainda mais para quem leu o <a href="obtendo_lua.html">tutorial
anterior</a>, que trata da instalação de Lua, e que menciona a compilação do
código-fonte da biblioteca Lua e dos programas <code>lua</code> e
<code>luac</code>.
</p>

<p>
No contexto da linguagem Lua, ou seja, ao falar em compilação <em>de
código Lua</em>, o significado do termo compilação muda bastante. Aqui não se
trata de gerar código em linguagem de máquina pronto para executar, e sim de
gerar <em>bytecode</em>, um formato intermediário entre a linguagem Lua e a
linguagem de máquina.
</p>

<p>
Lembra-se quando disse, no tutorial anterior, que a biblioteca Lua
transforma o código Lua em código de máquina, e que essa biblioteca é usada
pelo interpretador <code>lua</code>? Pois bem, essa biblioteca possui dentro
de si uma máquina virtual <em>bytecode</em>, e converte em linguagem de
máquina, para executar.
</p>

[% WRAPPER nota.tt titulo = "O que é uma máquina virtual?" %]
<p>
Talvez você já tenha visto o termo máquina virtual em outros
contextos, possivelmente com significados um pouco diferentes.
</p>

<p>
Não se trata aqui de emular um sistema operacional completo, e mesmo o
comparativo com máquinas virtuais de outros ambientes de
desenvolvimento seria potencialmente equivocado.
</p>

<p>
Vide <a href="../glossario.html#mquina-virtual">Máquina virtual</a>,
no glossário do Lu-a-Bá, para entender o conceito dentro da
terminologia de Lua.
</p>
[% END %]

<p>
Portanto, quando você informa um trecho ao interpretador, ele converte
primeiro esse trecho em <em>bytecode</em> e depois a máquina virtual lê o
<em>bytecode</em> gerado, para enfim transformar em linguagem de máquina e
executar.
</p>

<p>
Esse procedimento é repetido para cada trecho que você informa ao
interpretador. O que o compilador <code>luac</code> faz é antecipar essa
primeira parte, da conversão para o <em>bytecode</em>. Vamos ver como isso é
feito (novamente com o <code>astronauta.lua</code>).
</p>

[% WRAPPER codigo.tt %]
$ luac astronauta.lua
[% END %]

<p>
Feito isso, o arquivo <code>luac.out</code> terá sido gerado. Você não
conseguirá ler diretamente o seu conteúdo por se tratar de um formato
binário. Mas você pode executá-lo, como se ele fosse um arquivo-fonte Lua.
Veja:
</p>

[% WRAPPER codigo.tt %]
$ lua luac.out
Estou na Lua
Sinto falta da Terra
[% END %]

<p>
A princípio, pode não parecer muito diferente de uma execução direta,
porém nesse caso encurtamos o processo em uma etapa, o que faz diferença
quando trabalhamos com arquivos-fonte maiores e&#47;ou em maior
quantidade.
</p>

<p>
No entanto, é preciso dizer que a compilação Lua não afeta em nada o tempo
de execução do programa, apenas antecipa a conversão para o
<em>bytecode</em>.
</p>

<p>
Se preferir, você pode usar outro nome para o arquivo de saída, no lugar
de <code>luac.out</code>. Basta usar a opção <code>-o</code>.
</p>

[% WRAPPER codigo.tt %]
$ luac -o astronauta.out astronauta.lua
[% END %]

<p>
Isto é útil quando precisar gerar vários arquivos intermediários dentro de
um mesmo diretório. Ainda outra opção é não gerar arquivo de saída nenhum,
para isto utilizamos o parâmetro <code>-p</code>:
</p>

[% WRAPPER codigo.tt %]
$ luac -p astronauta.lua
[% END %]

<p>
E por que isso é útil? Porque se houver erros de sintaxe no arquivo-fonte,
o compilador vai avisar, permitindo que você identifique e corrija esses
erros. Caso nenhum erro seja detectado, o programa não emite nenhuma saída.
Caso encontre, informará a linha e uma breve mensagem de erro.
</p>

<p>
Essa técnica pode inclusive ser usada com arquivos de <em>bytecode</em>
gerados anteriormente, para se certificar de que são válidos e&#47;ou não
foram corrompidos.
</p>

[% WRAPPER codigo.tt %]
$ luac -p astronauta.out
[% END %]

<p>
É importante lembrar que <em>bytecodes</em> gerados para uma versão
específica de Lua não podem ser usados em outra versão. Sempre tenha o
arquivo-fonte original como garantia de que poderá gerar um <em>bytecode</em>
válido, quando necessário.
</p>

<h2 id="resumo">Resumo</h2>

<p>
Muito bem! Agora você já sabe como executar código Lua de três maneiras
diferentes:
</p>

<ul>
  <li>Interativamente, chamando o interpretador <code>lua</code> sem
  parâmetros;</li>

  <li>Via arquivos-fonte, passados como argumento para o interpretador;</li>

  <li>Como argumentos de linha de comando para o interpretador, usando a
  opção <code>-e</code>.</li>
</ul>

<p>
Também viu como combinar esses métodos de execução, usando a função
<code>dofile</code> e importações com a opção <code>-i</code> do
interpretador. Além disso, viu alguns exemplos de comentários em
arquivos-fonte, para facilitar o entendimento do código.
</p>

<p>
E ainda compreendeu o que é um processo de compilação de código Lua e
quando faz sentido usar essa técnica, com o programa <code>luac</code>.
</p>

[% INCLUDE adendos.tt
   principal = "executando_codigo_lua"
   adendos = [
     { hlink = "posix", titulo = "Execução em sistemas POSIX" }
     { hlink = "caracteres", titulo = "Codificação de caracteres" }
     { hlink = "comentarios", titulo = "Comentários aninhados" }
   ]
%]

<h2 id="contedo-relacionado">Conteúdo relacionado</h2>

<ul>
  <li>Bibliografia &#8211; <a href="../bibliografia/apostila_lua_2008.html">
    Lua &#8211; Conceitos básicos e API C</a>
  </li>

  <li>Bibliografia &#8211; <a href="../bibliografia/jai2009.html">Uma
  introdução à programação em Lua</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#bloco">Bloco</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#compilao-lua">Compilação
  Lua</a>
  </li>

  <li>Glossário &#8211; <a href=
  "../glossario.html#distribuio-padro">Distribuição padrão</a>
  </li>

  <li>Glossário &#8211; <a href=
  "../glossario.html#interpretador-lua">Interpretador Lua</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#mquina-virtual">Máquina
  virtual</a>
  </li>

  <li>Glossário &#8211; <a href="../glossario.html#trecho">Trecho</a>
  </li>

  <li>Blog &#8211; <a href="/blog/2024/fev/simples_casa_simples.html">Simples
  casa com simples</a>
  </li>
</ul>
[% END %]
