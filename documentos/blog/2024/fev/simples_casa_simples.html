[% META
   titulo = "Simples casa com simples"
   pchav  = "lua,português,blog"
%]
[% PROCESS blog.tt %]
[% WRAPPER artigo.tt %]
<p>Há algum tempo (desde o fim do ano passado) resolvi fazer um experimento:
trocar o editor <a href="https://www.vim.org/">vim</a> 
pelo vi (mais especificamente a implementação
<a href="https://repo.or.cz/nvi.git">nvi</a>), como editor para uso no
dia a dia, inclusive para programar em Lua.</p>

<p>Não entrarei no mérito do porquê dessa decisão, mas falarei um pouco sobre
a experiência que esse editor me proporcionou, e sobre o quanto ele me
surpreendeu na sua versatilidade, embora eu já o usasse há bastante tempo
para operações mais simplórias.</p>

<p>Após ler e reler o manual do vi algumas vezes, percebi que ele já possui
todos os recursos que preciso. Notadamente, ele permite:</p>

<ul>
  <li>Definir atalhos para sequências de ações;</li>

  <li>Criar abreviações para termos usados com frequência;</li>

  <li>Abrir um shell como subprocesso;</li>

  <li>Mostrar&#47;ocultar números das linhas;</li>

  <li>Mostrar mais de um arquivo (ou o mesmo) em telas divididas;</li>

  <li>Ler&#47;escrever em <em>buffers</em>;</li>

  <li>Desfazer e refazer ações múltiplas vezes (embora a forma de fazer isso
  seja um pouco diferente de como é feito no vim);</li>

  <li>Gerenciar e navegar por <em>tags</em>;</li>

  <li>Salvar preferências do editor em arquivo (geralmente no
  <code>.exrc</code>);</li>

  <li>Alternar entre preferências diferentes (carregando arquivos de
  preferências diferentes);</li>

  <li>Salvar e reutilizar histórico de comandos;</li>

  <li>Modo <code>ex</code> (edição não visual, por linha de comando);</li>

  <li>Dentre outros (que podem ser encontrados no manual do vi).</li>
</ul>

[% WRAPPER nota.tt titulo = "O que são tags no vi?" %]
<p>
O conceito de <em>tags</em> (etiquetas) aqui faz referência a
símbolos que fazem parte de um programa, como nomes de variáveis e
funções, por exemplo. As <em>tags</em> são uma forma de catalogar esses
símbolos para depois facilitar a pesquisa e navegação por
eles.
</p>
[% END %]

<p>Todo esse conjunto de ferramentas permite que se crie uma infinidade de
usos para o vi, combinando atalhos, comandos, macros em registradores, troca
de preferências, troca de modo (<code>vi</code> &#47; <code>ex</code>),
execução de comandos externos, etc.</p>

<p>Talvez você esteja se perguntando se não está faltando nada. Quase consigo
te imaginar dizendo algo como &#8220;mas e destaque de sintaxe, não
tem?&#8221;</p>

<p>Pois bem, aí você me pegou. Não tem. No entanto, eu pude perceber que isso
na verdade não é um problema. Pelo contrário, é quase um livramento. Imagine,
por exemplo, se para ler um texto (em português ou qualquer idioma que
conheça) as palavras fossem destacadas em cores diferentes (verbos em uma
cor, substantivos em outra, etc.).</p>

<p>Você acha que isso ajudaria ou atrapalharia na leitura? Pois bem, no
código é a mesma coisa. Destacar a sintaxe faz com que você preste mais
atenção à sintaxe e menos atenção à semântica (significado) do código, que é
o que efetivamente importa.</p>

<p>Pode até ser que esse recurso possua alguma relevância ainda durante os
primeiros passos com uma nova linguagem, quando ainda não se está muito
familiarizado. Porém eu ainda assim suspeito que não, pois lembro-me que na
faculdade eu tive o desprazer de escrever meus primeiros programas usando
papel e caneta, sem poder testá-los num primeiro momento.</p>

<p>Por mais que tenha sido um desprazer, acredito que em nada me prejudicou
no entendimento da sintaxe. E como você pode imaginar, papel e caneta não são
exatamente o ambiente ideal para destaque de sintaxe.</p>

<h2 id="ok-mas-e-lua">Ok, mas e Lua?</h2>

<p>Afinal, estamos aqui para falar sobre Lua, não é mesmo? Pois bem, o que
ocorre é que o editor vi tem muito a ver com Lua. Ele é simples e versátil, e
possui uma documentação que cabe na cabeça do programador.</p>

<p>Programar em Lua não requer nenhuma ferramenta ultra sofisticada. Não
requer nada especializado. E você pode adaptar o vi para definir
preferências, atalhos e abreviações que auxiliem na escrita de código
Lua.</p>

<p>Vou demonstrar alguns exemplos de adaptação aqui.</p>

<p>Vamos começar criando um arquivo de preferências específico para Lua, ou
seja, um conjunto de preferências para o vi que deverá permanecer ativo
quando estiver editando um arquivo-fonte Lua.</p>

<p>Por convenção, uso o diretório <code>~&#47;.config&#47;vi</code> para
salvar arquivos desse tipo, e também por convenção, uso a extensão
<code>.ex</code> no nome do arquivo, já que são basicamente comandos do modo
<code>ex</code> do editor. Portanto nesse caso o arquivo será
<code>~&#47;.config&#47;vi&#47;lua.ex</code> (mas você pode usar as
convenções que considerar apropriadas para o seu caso).</p>

<p>Para começar, vamos usar uma abreviação. Embora seja incomum que haja
longas sequências de palavras repetidas em Lua, existem algumas situações em
que abreviações podem ajudar. Por exemplo, no uso de funções locais. No lugar
de digitar <code>local function</code>, poderíamos digitar apenas
<code>lof</code> seguido de um espaço, de modo que será substituído por
<code>local function</code>.</p>

<p>Então para isso, adicionamos ao arquivo <code>lua.ex</code> a seguinte
linha:</p>

[% WRAPPER codigo.tt %]
abbrev lof local function
[% END %]

<p>Sinta-se a vontade para incluir outras abreviações que julgar úteis. Agora
vamos criar alguns atalhos. Digamos que você queira executar um arquivo-fonte
Lua ao pressionar F5 (lembre-se de salvar o arquivo antes). E que ao
pressionar F6 você valida que não haja erros de sintaxe.</p>

<p>Para isto, inclua as seguintes linhas (não copie e cole, em seguida
explico o porquê):</p>

[% WRAPPER codigo.tt %]
map ^[[15~ :!lua %^M
map ^[[17~ :!luac -p %^M
[% END %]

<p>Não se engane pela aparência dessas linhas, existem alguns caracteres
especiais nelas (formados por uma combinação entre <code>^</code> e o
caractere logo em seguida). Mantive essa representação (que também é usada no
vi) porque na realidade são caracteres não imprimíveis.</p>

<p>O caractere <code>^[</code> na verdade é um &#8220;Escape&#8221; (0x1b na
tabela ASCII), e o caractere <code>^M</code> representa uma combinação de
retorno (0x0d) seguido de nova linha (0x0a). Não se preocupe em decorar os
detalhes, explicarei como inserir esses caracteres especiais usando o próprio
vi.</p>

<p>Para inserir o caractere referente ao F5 no vi, primeiro digite a
combinação <code>Ctrl+v</code>, seguida da tecla F5, e o resultado será
<code>^[[15~</code>. O mesmo procedimento pode ser feito para F6 e&#47;ou
para quaisquer outras teclas de função. Para o retorno, seguido da nova
linha, digite <code>Ctrl+v</code>, dessa vez seguido de
<code>Enter</code>.</p>

<p>E agora explicando o significado dessas linhas, o comando <code>map</code>
recebe dois argumentos, sendo o primeiro a tecla ou combinação de teclas de
atalho, e o segundo é a sequência de caracteres que digitaria no vi. Neste
caso, usamos dois-pontos (<code>:</code>) para anunciar que um comando
<code>ex</code> será executado, depois exclamação (<code>!</code>) para
anunciar que um comando externo será executado. Na primeira linha, por
exemplo, chamamos o interpretador <code>lua</code>, seguido do sinal de
porcentagem (<code>%</code>), que é substituído pelo nome do arquivo.</p>

<p>Muito bem, atalhos criados, acho que agora já podemos testar, certo?
Errado. Ao abrir o editor vi, você ainda precisará importar as preferências,
para que elas façam efeito. A importação de arquivos de comandos
<code>ex</code> no vi pode ser feita com o comando <code>source</code>.
Porém, se não quiser digitar esse comando a cada vez que abrir um
arquivo-fonte Lua, pode definir um atalho para importação.</p>

<p>Para isto, vamos editar então o arquivo <code>~&#47;.exrc</code>, que
contempla as preferências globais do vi. Nele, adicionamos a seguinte
linha:</p>

[% WRAPPER codigo.tt %]
map gl :source ~&#47;.config&#47;vi&#47;lua.ex^M
[% END %]

<p>Pronto! Agora, quando abrir um arquivo Lua, basta digitar <code>g</code>
seguido de <code>l</code>, que a importação será feita. Isto habilitará todos
os atalhos e abreviações que tiver definido para trabalhar com Lua.</p>

<p>Isto é apenas uma demonstração de algumas possibilidades, porém com a
devida familiaridade com o editor vi e um pouco de imaginação, certamente
você encontrará inúmeras outras combinações que sejam do seu interesse, tanto
para editar arquivos-fonte Lua como para outros tipos de arquivo.</p>

<h2 id="referncias-externas">Referências externas</h2>

<p>Os textos a seguir tem relação com o artigo, e também podem ser do seu
interesse:</p>

<ul>
  <li>
    <a href=
    "https://framalistes.org/sympa/arc/gnl/2024-01/msg00004.html">Troquei o
    vim pelo vi</a>: breve relato pessoal que publiquei em lista de e-mail,
    no qual comento essa decisão de trocar o vim pelo vi;
  </li>

  <li>
    <a href="http://why-vi.rocks/">why vi rocks</a> (em inglês): uma lista de
    truques que podem ser aplicados usando o vi;
  </li>

  <li>
    <a href="https://www.romanzolotarev.com/vi.html">Edit text with vi(1)</a>
    (em inglês): outra lista de truques, que inclusive foi a primeira a
    despertar o meu interesse novamente pelo vi;
  </li>

  <li>
    <a href="https://hakon.gylterud.net/opinion/syntax-highlighting.html">How
    I program without syntax highlighting</a> (em inglês): texto que chama a
    atenção para a viabilidade (e as vantagens) de programar sem o uso de
    destaque de sintaxe;
  </li>

  <li>
    <a href="http://www.linusakesson.net/programming/syntaxhighlighting/">A
    case against syntax highlighting</a> (em inglês): outro texto que também
    fala sobre destaque de sintaxe, em que fala sobre o quanto a percepção de
    que esse recurso ajuda pode ser uma percepção equivocada.
  </li>
</ul>
[% END %]
