[% META
   titulo = "Documento: &#8220;Lua &#8211; Conceitos Básicos e API C&#8221;"
   h1id   = "documento-lua-conceitos-bsicos-e-api-c"
%]
[% PROCESS geral.tt %]
[% WRAPPER artigo.tt %]
<ul>
  <li>
    <strong>Ano:</strong> <a href="../cronologia.html#2008">2008</a>
  </li>

  <li><strong>Idioma:</strong> Português</li>

  <li>
    <strong>Baixar&#47;acessar:</strong>
    <ul>
      <li><a href="https://www.lua.org/doc/apostila_lua_2008.pdf">
      PDF</a> [ www.lua.org ]</li>
    </ul>
  </li>
</ul>

<p>Tutorial elaborado pela comunidade de desenvolvedores do (hoje
extinto) <a
href="http://web.archive.org/web/20230308221708/https://www.keplerproject.org/">
projeto Kepler</a>, em 2008.</p>

<h2 id="pblico-alvo">Público-alvo</h2>

<p>Este documento é útil tanto para quem deseja aprender a programar em Lua,
como para quem deseja embutir a biblioteca Lua em outros programas, usando a
API C. O documento pressupõe conhecimento prévio de programação em C
(sobretudo a partir do ponto que explica a API C), embora seja possível
usufruir dele sem esse conhecimento para aprender sobre a linguagem Lua, nos
capítulos iniciais.</p>

<h2 id="estrutura">Estrutura</h2>

<p>A apostila se divide em 9 capítulos:</p>

<ul>
  <li>Capítulo 1: <strong>Lua</strong></li>

  <li>Capítulo 2: <strong>Funções</strong></li>

  <li>Capítulo 3: <strong>Cadeias de caracteres</strong></li>

  <li>Capítulo 4: <strong>Tabelas</strong></li>

  <li>Capítulo 5: <strong>Módulos</strong></li>

  <li>Capítulo 6: <strong>API C</strong></li>

  <li>Capítulo 7: <strong>Usando Lua em uma aplicação C</strong></li>

  <li>Capítulo 8: <strong>Usando Funções C em Lua</strong></li>

  <li>Capítulo 9: <strong>Definindo Novos Tipos de Dados em C</strong></li>
</ul>

<h3 id="resumo">Resumo</h3>

<p>O <strong>capítulo 1</strong> apresenta os principais elementos da
linguagem Lua que um programador poderia buscar saber para iniciar sua
jornada, sem aprofundar muito, mas já permitindo um contato inicial. Alguns
conceitos como os tipos de dados (simples e complexos), alguns operadores e
estruturas de controle e repetição são apresentados neste primeiro
capítulo.</p>

<p>O <strong>capítulo 2</strong> detalha um pouco mais as pecularidades das
funções dentro do ambiente de desenvolvimento Lua, tal como a possibilidade
de múltiplos retornos (que inclusive podem variar entre chamadas), o fato de
serem valores de primeira classe e como isso pode ser explorado para
construir fechos, o fato de poderem ser anônimas, dentre outros detalhes
particulares em Lua.</p>

<p>O <strong>capítulo 3</strong> apresenta o conceito de cadeia de caracteres
(<em>string</em>) em Lua, como um dos tipos básicos, e algumas funções
presentes na biblioteca padrão que tem a finalidade de manipular estas
cadeias.</p>

<p>O <strong>capítulo 4</strong> fala sobre tabelas, algumas operações
básicas (inserção e remoção de itens, ver tamanho), e também algumas formas
de usar esse tipo para algumas finalidades, como descrição de dados e até
mesmo uma forma de orientação a objetos, fazendo uso do conceito de
metatabelas e metamétodos, que dão grande versatilidade às tabelas.</p>

<p>O <strong>capítulo 5</strong> explica o sistema de módulos, que
reaproveita vários conceitos nativos da linguagem para estruturar
bibliotecas. Este capítulo cobre também a criação, instalação e o uso de
módulos.</p>

<p>O <strong>capítulo 6</strong> inicia a explicação sobre a API C de Lua,
apresentando o conceito da pilha virtual que faz a comunicação entre o código
C e o código Lua, separando tanto o sistema de tipos como o controle de
memória, visto que ambos diferem nas duas linguagens. Alguns exemplos de
código são utilizados para demonstrar as funções de criação, consulta e
manipulação dessa pilha.</p>

<p>O <strong>capítulo 7</strong> mostra uma das aplicabilidades da integração
entre C e Lua, fazendo uso da API. Neste caso, usa-se a um arquivo Lua para
configurar uma aplicação principal (escrita em C). Portanto, a linguagem Lua
neste cenário é usada para descrição de dados.</p>

<p>Ainda assim, fica evidente que, para além de um arquivo de configuração
convencional, esta abordagem permite incluir até mesmo funções no código Lua
que serão chamadas a partir do programa principal, o que também caracteriza
Lua como uma linguagem de extensão.</p>

<p>O <strong>capítulo 8</strong> trata sobre funções C exportadas para o
ambiente Lua. Ou seja, se por um lado o capítulo anterior fala sobre execução
de código Lua em C, este capítulo fala sobre execução de código C em Lua.
Primeiro ele mostra o protocolo a ser seguido para criar funções desse tipo,
e em seguida como funções C podem ser agrupadas em uma biblioteca para uso em
Lua.</p>

<p>Ainda neste capítulo se fala também sobre a criação de funções C que
guardam estado, isto é, funções que trabalham com valores persistidos entre
chamadas, utilizando de três métodos diferentes: usando um registro global
disponível pela biblioteca para uso em código C, usando ambientes e usando
variáveis locais externas (ou <em>upvalues</em>).</p>

<p>O <strong>capítulo 9</strong> ensina a criar tipos de usuário específicos,
ou seja, tipos de dados que são definidos em C, porém acessíveis em Lua, por
uma interface pré-definida. Ele começa apresentando um exemplo básico e
depois ajustando para utilizar metamétodos (o que permite uma abordagem
orientada a objetos).</p>

<p>Por fim ele mostra uma terceira abordagem alternativa, que separa o tipo C
e funções associadas de uma interface Lua que apenas reaproveita essa
estrutura pré-existente. Neste caso, funções Lua encapsulam as funções C
existentes e são associadas ao tipo (<em>userdata</em>) criado em Lua.</p>

<h2 id="comentrios">Comentários</h2>

<p>O nome &#8220;Lua&#8221; é bastante apropriado porque para um observador
terrestre, o satélite natural homônimo possui duas faces, uma visível e outra
oculta. Neste sentido, é comum abordar Lua mencionando a sua face visível
(uma linguagem de alto nível), porém esquecer sua face oculta (uma biblioteca
C), que são indissociáveis e igualmente importantes. Este é um ótimo material
introdutório, que explora por igual esses dois lados.</p>

[% WRAPPER nota.tt titulo = "Ou seria a face oculta a linguagem?" %]
<p>
Talvez do ponto de vista do código C a melhor analogia seria o
contrário, isto é, a face oculta sendo a linguagem e a face visível a
biblioteca, já que o código C interage com o código Lua por meio da API
C da biblioteca Lua.
</p>
[% END %]

<p>O documento pode ser dividido logicamente em três partes. Do capítulo 1 ao
4, são tratados assuntos relativos apenas à linguagem Lua em si. No meio, o
capítulo 5 ainda se refere à linguagem, porém já menciona questões
relacionadas à implementação, em especial por tratar sobre módulos,
diretórios padrão e variáveis de ambiente.</p>

<p>Do capítulo 6 ao 9 o tema central passa a ser a API Lua, seguindo um
roteiro relativamente comum ao abordar esse assunto, que passa por quatro
tópicos principais: apresentação da pilha virtual, execução de código Lua
dentro da aplicação C, criação de bibliotecas C para execução a partir do
código Lua e por fim a criação de novos tipos definidos em C para uso em Lua,
usando o tipo <em>userdata</em>.</p>

<p>Sobre o capítulo 5 uma ressalva deve ser feita: embora não esteja
totalmente desatualizado, e ainda tenha relevância para a explicação sobre o
funcionamento de módulos, houve uma mudança significativa na maneira como
eles funcionam a partir da versão 5.2,em parte por causa da <a href=
"https://www.lua.org/manual/5.2/pt/manual.html#8.1">mudança no conceito de
ambientes</a> e da convenção de que <a href=
"https://www.lua.org/manual/5.2/pt/manual.html#8.2">módulos não sejam
variáveis globais</a> a partir de então.</p>

<p>É importante frisar também que o conceito de ambientes não foi abordado
neste documento, porém citado no capítulo 5, para explicar a função
<code>module()</code>. Ocorre que este é um conceito importante para uma
compreensão mais abrangente sobre módulos. Depois, no capítulo 8, é citado
novamente, em referência a um dos métodos utilizados para criar funções C com
estado em Lua.</p>

<p>Além do conceito de ambientes, outro tópico que não foi abordado neste
documento é o mecanismo de co-rotinas, que é um dos pilares da linguagem,
embora programas mais simples raramente façam uso desse mecanismo. E ainda
outro conceito que ficou de fora foi a interface de depuração, que oferece
diversos mecanismos para inspecionar e manipular internamente um programa
Lua, a fim de identificar possíveis falhas ou comportamentos
imprevisíveis.</p>

<p>Evidentemente, esse material é proveitoso para mais de um público-alvo.
Para quem deseja apenas programar em Lua, seja para construir pequenos
programas ou realizar configurações ou extensões de programas que já utilizam
Lua, os capítulos 1 a 5 serão de maior interesse. Para quem deseja integrar
Lua a uma outra aplicação, biblioteca ou sistemas embarcados, a atenção maior
estará sobre os capítulos 6 a 9.</p>

<h2 id="pontos-de-ateno">Pontos de atenção</h2>

<ul>
  <li>
    <p>No <strong>capítulo 1</strong> (página 5) parece haver um erro no
    terceiro parágrafo, no trecho “o acesso a variáveis locais é bastante
    mais rápido do que o acesso a variáveis <strong>locais</strong>”.
    Aparentemente a intenção do autor para o segundo uso da palavra “locais”
    (aqui grifado) seria na verdade <strong>globais</strong>.</p>
  </li>

  <li>
    <p>O <strong>capítulo 1</strong> (página 12) apresenta um exemplo de uso
    da estrutura condicional <em>if</em>, que já utiliza também as cláusulas
    <em>elseif</em> e <em>else</em>, sem que estas tenham sido previamente ou
    posteriormente explicadas. Um leitor mais atento ou já familiarizado com
    programação não terá problemas para compreender o que isso significa.</p>

    <p>De todo modo, para maior clareza, a cláusula <em>elseif</em> determina
    o que será feito caso a primeira condição não seja atendida,
    estabelecendo uma nova condição, enquanto a cláusula <em>else</em>
    abrange todas as condições que não as especificadas em <em>if</em> e
    <em>elseif</em> de um mesmo bloco.</p>
  </li>

  <li>
    <p>No <strong>capítulo 3</strong> (página 19) parece haver outro
    equívoco, no trecho &#8220;Caso o primeiro parâmetro de
    <em>string.sub</em> seja negativo&#8221;, pois o primeiro parâmetro não é
    numérico e sim a própria cadeia a ser manipulada, salvo quando essa
    função é chamada na notação de método, conceito que até esse trecho ainda
    não havia sido explicado.</p>
  </li>

  <li>
    <p>No <strong>capítulo 5</strong> (página 31), são enumerados os módulos
    que compõem a biblioteca padrão. Quanto a isto, cabe notar que a versão
    5.2 (que é posterior a este documento) <a href=
    "https://www.lua.org/manual/5.2/readme.html#changes">oferece um novo
    módulo</a> (<em>bit32</em>), para <a href=
    "https://www.lua.org/manual/5.2/pt/manual.html#6.7">suporte a operações
    bit a bit</a>. Este módulo depois é <a href=
    "https://www.lua.org/manual/5.3/manual.html#8.2">depreciado</a> em favor
    de operadores nativos, na versão 5.3, e efetivamente removido na versão
    5.4.</p>

    <p>Além dele, <a href=
    "https://www.lua.org/manual/5.3/readme.html#changes">surge também um novo
    módulo</a> (<em>utf8</em>) na versão 5.3, que adiciona um <a href=
    "https://www.lua.org/manual/5.3/manual.html#6.5">suporte básico a
    UTF-8</a> em Lua.</p>
  </li>

  <li>
    <p>No <strong>capítulo 5</strong> (página 32), a função <em>module</em> é
    citada, como um mecanismo padronizado para construção de novos módulos.
    Entretanto, essa função, que surgiu na versão 5.1 <a href=
    "https://www.lua.org/manual/5.2/pt/manual.html#8.2">caiu em
    depreciação</a> na versão 5.2, e a partir da versão 5.3 foi efetivamente
    removida.</p>
  </li>

  <li>
    <p>No <strong>capítulo 5</strong> (página 32), a função <em>require</em>
    é utilizada de um modo que pressupõe que o módulo criará uma tabela em
    escopo global, com o nome do módulo em questão. De fato, isto ocorria com
    módulos criados utilizando a função <em>module</em>. Entretanto,
    atualmente essa premissa <a href=
    "https://www.lua.org/manual/5.2/pt/manual.html#8.2">já não
    necessariamente pode ser considerada verdadeira</a>.</p>
  </li>

  <li>
    <p>O <strong>capítulo 5</strong> (página 34) menciona as variáveis de
    ambiente PATH e CPATH, usadas para carregar módulos Lua e módulos C. No
    entanto, é bom observar que essas variáveis não são lidas diretamente por
    <em>require</em>. Em vez disso, elas são usadas para configurar as
    variáveis
    <a href="https://www.lua.org/manual/5.1/pt/manual.html#pdf-package.path"><code>package.path</code></a>
    e
    <a href="https://www.lua.org/manual/5.1/pt/manual.html#pdf-package.cpath"><code>package.cpath</code></a>,
    respectivamente. Este comportamento já se observa na versão 5.1.</p>
  </li>

  <li>
    <p>O <strong>capítulo 6</strong> (página 38) utiliza um exemplo de código
    no qual se espera que as funções <code>luaL_loadbuffer()</code> e
    <code>lua_pcall()</code> retornem um valor verdadeiro para trechos e
    execuções sem erros, e um valor falso, caso contrário.</p>

    <p>Esta construção permanece válida, mas convém notar que a partir da
    versão 5.2, há <a href=
    "https://www.lua.org/manual/5.2/pt/manual.html#lua_pcall">uma macro
    definida no cabeçalho <em>lua.h</em></a> chamada <code>LUA_OK</code>
    (igual a zero) que pode ser usada para comparar o resultado destas duas
    funções. Isto é uma boa prática, considerando que o valor esperado poderá
    mudar em versões futuras (embora esse tipo de mudança seja pouco provável
    sem um bom motivo).</p>
  </li>

  <li>
    <p>No <strong>capítulo 7</strong> (página 47) um trecho de código logo no
    início da página mostra um exemplo de obtenção de um campo de uma tabela.
    No final deste trecho de código, há uma chamada à função
    <code>lua_pop()</code>, na qual ficou faltando o primeiro argumento (isto
    é, uma referência à pilha virtual).</p>

    <p>Isto fica mais claro quando, posteriormente, um exemplo de abstração
    deste exemplo na função <code>c_getfield()</code> (na mesma página)
    mostra a chamada a <code>lua_pop()</code> novamente, mas desta vez com a
    parametrização correta.</p>
  </li>

  <li>
    <p>No <strong>capítulo 8</strong> (página 52) há um exemplo de uso da
    função <code>luaL_register()</code>. Na versão 5.1, que era a mais atual
    quando este documento foi elaborado, este era o método empregado para
    registrar novas bibliotecas por meio da API. No entanto, a partir da
    versão 5.2, <a href=
    "https://www.lua.org/manual/5.2/pt/manual.html#8.3">essa função foi
    tornada obsoleta</a>, tendo em vista que novos módulos já não criariam
    objetos em escopo global, por padrão.</p>

    <p>Para obter resultado semelhante, deve-se usar a função <a href=
    "https://www.lua.org/manual/5.2/pt/manual.html#luaL_newlib"><code>luaL_newlib()</code></a>,
    com a diferença que um nome de biblioteca não é especificado. Em vez
    disso, quando a função <code>require()</code> for usada em Lua para obter
    o módulo, uma tabela com as funções registradas será retornada.</p>
  </li>

  <li>
    <p>No <strong>capítulo 8</strong> (páginas 53 e 54) um exemplo é dado com
    o uso do pseudo-índice <code>LUA_ENVIRONINDEX</code>, para criar uma
    variável atrelada a um ambiente acessível por funções C, mas não exposta
    no contexto do programa Lua. Isto funciona na versão 5.1, porém a partir
    da versão 5.2, <a href=
    "https://www.lua.org/manual/5.2/pt/manual.html#8.3">funções C não possuem
    mais ambientes</a>, logo esse pseudo-índice não existe mais.</p>

    <p>Como solução, é possível utilizar a função <a href=
    "https://www.lua.org/manual/5.2/pt/manual.html#luaL_setfuncs"><code>luaL_setfuncs()</code></a>
    para definir uma quantidade de <em>upvalues</em> comum (embora não
    compartilhado) a todas as funções. Para que compartilhar
    <em>upvalues</em> entre diferentes funções C, convém utilizar uma tabela
    que agrupe todos os valores a serem compartilhados.</p>
  </li>

  <li>
    <p>No <strong>capítulo 8</strong> (página 55) há um trecho de código que
    usa uma função <code>luaL_checkint()</code> (na linha 10,
    especificamente), que não existe. Provavelmente o que se pretendia nesse
    caso era usar a função <code>luaL_checkinteger()</code>.</p>
  </li>

  <li>
    <p>No <strong>capítulo 9</strong> (páginas 61 e 62), há um exemplo que
    mostra o uso da função <code>luaL_register()</code>, que conforme já foi
    comentado, <a href=
    "https://www.lua.org/manual/5.2/pt/manual.html#8.3">foi tornada obsoleta
    na versão 5.2</a>. Neste exemplo específico, ela é usada para vincular os
    metamétodos criados em C aos nomes correspondentes na metatabela do tipo
    criado.</p>

    <p>Para obter o mesmo efeito a partir de então, será necessário usar a
    função
    <a href="https://www.lua.org/manual/5.2/pt/manual.html#luaL_setfuncs"><code>luaL_setfuncs()</code></a>,
    que associa o conjunto de funções informado a uma tabela existente (e no
    topo da pilha).</p>
  </li>
</ul>

<h2 id="contedo-relacionado">Conteúdo relacionado</h2>

<ul>
  <li>
    Glossário &#8211; <a href="../glossario.html#api-c">API C</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#ambiente">Ambiente</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#ambiente-global">Ambiente
  Global</a>
  </li>

  <li>
    Glossário &#8211; <a href=
  "../glossario.html#biblioteca-auxiliar">Biblioteca auxiliar</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#biblioteca-padro">Biblioteca
  padrão</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#bloco">Bloco</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#compilao-lua">Compilação
  Lua</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#chamada-protegida">Chamada
  protegida</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#coletor-de-lixo">Coletor de
  lixo</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#escopo-lxico">Escopo
  Léxico</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#estado-lua">Estado Lua</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#fecho">Fecho</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#finalizador">Finalizador</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#funo-annima">Função
  anônima</a>
  </li>

  <li>
    Glossário &#8211; <a href=
  "../glossario.html#interpretador-lua">Interpretador Lua</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#mquina-virtual">Máquina
  Virtual</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#metamtodo">Metamétodo</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#metatabela">Metatabela</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#mdulo">Módulo</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#pilha-virtual">Pilha
  virtual</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#tabela">Tabela</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#trecho">Trecho</a>
  </li>

  <li>
    Glossário &#8211; <a href="../glossario.html#userdata">Userdata</a>
  </li>

  <li>
    Glossário &#8211;
    <a href="../glossario.html#valores-de-primeira-classe">
    Valores de primeira classe</a>
  </li>

  <li>
    Glossário &#8211;
    <a href="../glossario.html#varivel-global">Variável global</a>
  </li>

  <li>
    Glossário &#8211;
    <a href="../glossario.html#varivel-local">Variável local</a>
  </li>

  <li>
    Glossário &#8211;
    <a href="../glossario.html#varivel-local-externa">Variável local externa</a>
  </li>

  <li>
    Tutoriais &#8211;
    <a href="../tutoriais/executando_codigo_lua.html">Executando código Lua</a>
  </li>
</ul>
[% END %]
