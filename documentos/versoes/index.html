[% META
   titulo = "Versões"
%]
[% PROCESS geral.tt %]
[% WRAPPER artigo.tt %]
<p>A <a href="https://www.lua.org/versions.html">distribuição padrão</a> de
Lua é, como na maior parte dos programas de computador, versionada. Cada nova
versão traz mudanças que podem ser a remoção de algo que não faz mais
sentido, a correção de um problema, a melhoria de um recurso existente ou um
novo recurso.</p>

<p>As versões de <code>lua</code> seguem o formato <em>X</em>.<em>Y</em> (por
exemplo, <em>5.0</em>). Para uma mesma versão, diferentes lançamentos podem
ser realizados para correções de problemas.</p>

<p>Lançamentos diferentes seguem o padrão <em>X</em>.<em>Y</em>.<em>Z</em>,
onde <em>Z</em> é o número do lançamento. Diferentes versões não oferecem
nenhuma garantia de compatibilidade binária. Por outro lado, lançamentos de
uma mesma versão são compatíveis.</p>

<p>Não é possível saber com precisão quais versões de Lua são utilizadas
atualmente, porém ao procurar por projetos que utilizam Lua veremos uma
preponderância de quatro versões suportadas: <em>5.1</em>, <em>5.2</em>,
<em>5.3</em> e <em>5.4</em>.</p>

<p>Isto não é por acaso. Além de serem as quatro versões mais recentes, todas
as quatro possuem a <a href="https://www.lua.org/docs.html">especificação</a>
ainda disponível na página principal do projeto Lua, e ainda são oferecidas
em repositórios de vários sistemas operacionais, o que dá às quatro versões
um estado não oficial de &#8220;versão corrente&#8221;. Na prática, porém,
cabe destacar que apenas a versão <em>5.4</em> ainda segue em
desenvolvimento.</p>

<p>Embora existam algumas mudanças de uma versão para outra, é possível
escrever código que funcione para as quatro versões, quando isso for
desejável. Geralmente isso é adequado quando se deseja migrar de uma versão
mais antiga para uma versão mais nova (já que o contrário é no mínimo
incomum).</p>

<p>Seja como for, saber o que mudou de uma versão para outra é importante,
especialmente entre essas quatro versões mais utilizadas. Essas mudanças são
muito bem documentadas nas especificações de cada versão. Portanto o foco
deste documento não é exatamente elencar o que mudou, mas comentar algumas
consequências dessas mudanças e cuidados que o programador deve ter, em vista
dessas mudanças.</p>

<h2 id="5.4">5.4</h2>

<ul>
  <li>Ano de primeiro lançamento: <a href="../cronologia.html#2020">2020</a>
  </li>

  <li>Ano do lançamento mais recente: 2023 (Lua 5.4.6)</li>
</ul>

<p>Algumas das grandes mudanças na versão <em>5.4</em> foram
&#8220;internas&#8221;, ou seja, sem grandes consequências visíveis na
linguagem Lua e na API C. A exemplo disto temos agora um novo método para
coleta de lixo (geracional), que já havia sido planejada para versões
anteriores, porém acabou se tornando viável apenas nesta versão. Outra
mudança interna foi o uso de um novo algoritmo para a função
<code>math.random()</code>.</p>

<p>Porém houve também importantes mudanças mais visíveis. Exemplo disto é a
manipulação de co-rotinas que agora permite explicitamente fechar uma
co-rotina via biblioteca coroutine em Lua, e reiniciar uma co-rotina ao seu
estado inicial via API. Outra grande mudança é a inclusão de constantes e
variáveis &#8220;fecháveis&#8221;, que em conjunto com as funções de
manipulação de co-rotina trazem novas possibilidades para a programação
funcional em Lua.</p>

<p>Dentre as mudanças na API, podemos destacar também a possibilidade de
múltiplos valores Lua embutidos em um <em>userdata</em> (<em>user
values</em>). Antes apenas um <em>uservalue</em> era possível. Isso mudou um
pouco as funções de configuração e acesso a <em>user values</em>.</p>

<h3 id="mais-informaes">Mais informações</h3>

<ul>
  <li>
    <a href="https://www.lua.org/manual/5.4/readme.html#changes">README</a>
    (seção de principais mudanças, em inglês).
  </li>

  <li>Lista detalhada de <a href=
  "https://www.lua.org/manual/5.4/manual.html#8">incompatibilidades</a> (em
  inglês) com a versão anterior.
  </li>
</ul>

<h2 id="5.3">5.3</h2>

<ul>
  <li>Ano de primeiro lançamento: <a href="../cronologia.html#2015">2015</a>
  </li>

  <li>Ano do último lançamento: 2020 (Lua 5.3.6)</li>
</ul>

<p>Na versão <em>5.3</em> a biblioteca <em>bit32</em> que havia surgido na
versão anterior é depreciada, tendo em vista que agora existem operadores que
implementam a mesma funcionalidade, tornando mais prático a execução de
operações bit a bit. E surge também a biblioteca <em>utf8</em>, que adiciona
um suporte básico a manipulação de caracteres UTF-8.</p>

<p>Outro destaque é a representação de números como inteiros de 64 bits por
padrão. Apenas quando há uma parte fracionária, os números passam a ser
representados por tipos de ponto flutuante. Cabe destacar que os operadores
bit a bit surgidos nesta versão operam sobre uma representação de inteiros de
64 bits, diferente da biblioteca <em>bit32</em>, que operava sobre inteiros
de 32 bits.</p>

<p>Outra novidade interessante é a possibilidade de associação de valores Lua
aos tipos <em>userdata</em>. Isso dá muito mais flexibilidade e versatilidade
a esses tipos, que agora podem usufruir dos tipos Lua como parte de si,
provendo uma interface mais natural com as partes do programa escritas em
Lua.</p>

<p>Também é possível realizar serialização e de-serialização com as novas
funções <code>string.pack()</code> e <code>string.unpack()</code>, que
manipulam um conjunto de bytes em uma cadeia de caracteres de maneira
compacta e com padrões de acesso bem definidos.</p>

<p>Por fim, mas não menos importante, há um conjunto de mudanças que melhoram
a ergonomia da programação com Lua, tanto na API (com as novas funções
<code>lua_seti()</code> e <code>lua_geti()</code>, para acesso a tabelas, por
exemplo), como na biblioteca padrão (com a nova função
<code>table.move()</code> e a não necessidade de asteriscos em
<code>io.read()</code>, por exemplo), além do novo operador de divisão
arredondada para baixo (<code>&#47;&#47;</code>).</p>

<h3 id="mais-informaes-1">Mais informações</h3>

<ul>
  <li>
    <a href="https://www.lua.org/manual/5.3/readme.html#changes">README</a>
    (seção de principais mudanças, em inglês).
  </li>

  <li>Lista detalhada de <a href=
  "https://www.lua.org/manual/5.3/manual.html#8">incompatibilidades</a> (em
  inglês) com a versão anterior.
  </li>
</ul>

<h2 id="5.2">5.2</h2>

<ul>
  <li>Ano de primeiro lançamento: <a href="../cronologia.html#2011">2011</a>
  </li>

  <li>Ano do último lançamento: <a href="../cronologia.html#2015">2015</a>
  (Lua 5.2.4)
  </li>
</ul>

<p>A versão <em>5.2</em> não é marcada por poucas grandes mudanças, mas sim
por várias pequenas mudanças que juntas compõem uma grande mudança relevante.
Talvez o maior destaque seja para o conceito de ambientes, que já existia em
versões anteriores, mas que assume uma nova denotação.</p>

<p>Ambientes passam a não existir mais para funções C, apenas funções Lua, e
são armazenados na tabela <code>_ENV</code>. Manipular essa tabela altera o
ambiente, de modo que as funções <code>getfenv()</code> e
<code>setfenv()</code> se tornam obsoletas. O ambiente global é compartilhado
por padrão para toda nova função sob a forma dessa tabela <code>_ENV</code>,
determinando o que estará disponível para a função em questão.</p>

<p>Essa mudança tem consequências também para a construção de módulos, que
agora pode fazer uso da tabela <code>_ENV</code> em Lua. Em C, não é mais
possível utilizar ambientes, porém ainda é válido utilizar tabelas como
<em>upvalues</em> compartilhados entre as funções de um módulo.</p>

<p>Neste sentido, a função <code>module()</code> foi depreciada em favor de
uma abordagem mais genérica, utilizando os recursos da linguagem (tabelas,
escopo léxico), além do fato de que se convencionou a partir de então que
módulos não criem objetos globais.</p>

<p>Outra mudança maior foi a criação do módulo <em>bit32</em> na biblioteca
padrão, de modo que Lua passou a ter suporte a operações de bit a bit em
inteiros de 32 bits com sinal. Para além disso, a função
<code>loadstring()</code> foi depreciada, uma vez que a função
<code>load()</code> passa a trabalhar também com cadeias de caracteres como
argumentos para definição dos trechos que executará.</p>

<h3 id="mais-informaes-2">Mais informações</h3>

<ul>
  <li>
    <a href="https://www.lua.org/manual/5.2/readme.html#changes">README</a>
    (seção de principais mudanças, em inglês).
  </li>

  <li>Lista detalhada de <a href=
  "https://www.lua.org/manual/5.2/manual.html#8">incompatibilidades</a> (em
  inglês) com a versão anterior. Disponível também <a href=
  "https://www.lua.org/manual/5.2/pt/manual.html#8">em português</a>.
  </li>
</ul>

<h2 id="5.1">5.1</h2>

<ul>
  <li>Ano de primeiro lançamento: <a href="../cronologia.html#2006">2006</a>
  </li>

  <li>Ano do último lançamento: <a href="../cronologia.html#2012">2012</a>
  (Lua 5.1.5)
  </li>
</ul>

<p>Dentre os principais destaques desta versão estão um novo esquema de
coleta de lixo incremental, que ajudou a prevenir contra a incidência de
longas pausas, e um novo sistema de módulos, que de certo modo padronizou
ainda mais a criação de módulos em Lua (utilizando a função
<code>module()</code>).</p>

<p>É importante frisar, no entanto, que esse esquema de criação de módulos
não durou muito, (vide a <a href="#5-2">versão 5.2</a>, sobre a depreciação
da função <code>module()</code>).</p>

<p>Algumas outras novidades importantes foram uma nova semântica para funções
com número arbitrário de argumentos (<em>varargs</em>), um novo operador para
cálculo do resto de divisões (operador <em>modulo</em>, ou seja,
<code>%</code>), comentários longos e cadeias longas que podem ser aninhados,
usando diferentes níveis, e metatabelas em tipos que não são tabelas (como
cadeias de caracteres e <em>userdata</em>).</p>

<h3 id="mais-informaes-3">Mais informações</h3>

<ul>
  <li>Lista detalhada de <a href=
  "https://www.lua.org/manual/5.1/manual.html#7">incompatibilidades</a> (em
  inglês) com a versão anterior. Disponível também <a href=
  "https://www.lua.org/manual/5.1/pt/manual.html#7">em português</a>.
  </li>
</ul>

<h2 id="5.0">5.0</h2>

<ul>
  <li>Ano de primeiro lançamento: <a href="../cronologia.html#2003">2003</a>
  </li>

  <li>Ano do último lançamento: <a href="../cronologia.html#2006">2006</a>
  (Lua 5.0.3)
  </li>
</ul>

<p>A versão <em>5.0</em> não é objeto de estudo do projeto Lu-a-Bá.
Entretanto, essa versão foi um marco importante na história da linguagem. Foi
uma grande mudança, que iniciou um novo ciclo de desenvolvimento de maior
maturidade na linguagem, o qual permanece em andamento até hoje. Além disso,
a versão <em>5.0</em> é mencionada em muitos dos materiais discutidos no
Lu-a-Bá. Deste modo, ela também é destacada aqui.</p>

<p>Dentre algumas das grandes mudanças podemos elencar o suporte total a
escopo léxico, co-rotinas, o novo mecanismo de laços <code>for</code>
genéricos, metatabelas e metamétodos, comentários longos (isto é, de blocos),
o tipo <em>booleano</em> e uma padronização sobre a distribuição de módulos
usando tabelas Lua.</p>

<p>Outro aspecto interessante a se observar é que esta foi a primeira versão
de Lua a utilizar a licença MIT. Versões anteriores (desde a versão 2.1) já
utilizavam uma licença permissiva (e também compatível com a GPL), porém era
uma licença personalizada, o que causou bastante controvérsia. Quando
reconhecida como compatível pela <em>Free Software Foundation</em>, a mudança
para a licença MIT já havia sido conduzida.</p>

<p>Para mais informações sobre o trajeto seguido desde a concepção inicial da
linguagem até o momento atual, vale conferir também a <a href=
"../bibliografia/hopl3.html">apresentação da evolução de Lua</a> no HOPL
III.</p>
[% END %]
