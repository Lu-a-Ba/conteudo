# Convenções e contribuição

Este documento trata das convenções do projeto.

## Visão geral

Neste repositório consta o material que será publicado nas páginas do
Lu-a-Bá, isto é, aqui fica o conteúdo propriamente. Portanto, as
orientações presentes neste documento valem especificamente para a parte
de redação do projeto.

## Controle de versões

O projeto é versionado com *git* (neste repositório), e conta com duas
ramificações (*branches*):

* **editando**: Versão em desenvolvimento, novo conteúdo é adicionado
  primeiro aqui;
* **publicado**: Versão principal, o conteúdo visível nas páginas do
  Lu-a-Bá deve corresponder a esta versão.

O repositório está hospedado no [Codeberg](https://codeberg.org). 

## Arquivos e diretórios padrão

O repositório é composto pela seguinte estrutura:

* `documentos`: Neste diretório encontram-se os documentos ainda não
  processados (modelos de documento), onde está o conteúdo, de fato,
  junto com imagens e folhas de estilo.

* `esqueleto`: Neste diretório encontram-se modelos complementares, como
  cabeçalhos e rodapés, e informações que constarão em várias páginas.

* `tmp`: Diretório temporário, ignorado pelo versionamento (consta no
  `.gitignore`), usado apenas para auxiliar o processo de construção das
  páginas HTML.

* `www`: Diretório de destino para as páginas HTML geradas. Também é ignorado
  (ou seja, está no `.gitignore`), pois o resultado final não faz parte deste
  repositório.

* `depend.txt`: Define, para cada documento a ser processado, quais são
  as suas dependências, de modo que se essas dependências forem alteradas,
  o documento também será reprocessado.

* `ttreerc`: Arquivo de configuração do utilitário `ttree`, usado para
  processar os documentos.

## Convenções de redação

* O texto deve ser claro e conciso, evitando repetir termos ou ideias dentro
  do mesmo contexto;

* Estrangeirismos devem ser evitados tanto quanto possível, e termos em inglês
  sem tradução deverão ser grifados e seu significado explicado. Quando
  possível, termos em português devem ser preferidos.

* Depois de elaborado, todo texto deve passar por uma revisão geral, isto é,
  do início ao fim. Esse procedimento deve se repetir toda vez que o texto
  passar por uma modificação significativa.

* É preferível que a revisão do texto seja feita já na página Web, isto é, uma
  instância local de testes deve ser usada para ler a página como se ela já
  tivesse sido publicada.

## Convenções Web

* As páginas devem prezar pela simplicidade e clareza, tornando a leitura mais
  confortável possível. Elementos desnecessários e distrações não devem constar
  nas páginas.

* O uso de *hiperlinks* no meio do texto deve ser feito de maneira comedida.
  Quanto menos deles, melhor, para evitar que o leitor se distraia com a
  navegação e consiga manter o foco na leitura. O ideal é, quando possível,
  utilizar referências e uma seção de conteúdo relacionado ao fim do texto.

* É preferível uma folha de estilos simples e enxuta a uma ou mais folhas
  complexas que precisem ser minificadas para não impactar no tempo de
  carregamento. Idealmente a minificação será desnecessária.

* Deve-se usar apenas fontes do sistema, preferencialmente orientando para
  famílias de fontes, deixando a critério do que estiver configurado no
  cliente. Ou seja, não se deve induzir o *download* de fontes para
  visualização do conteúdo.

* O uso de javascript é **expressamente** proibido no projeto. Navegadores sem
  suporte a javascript ou com esse "recurso" desativado deverão visualizar o
  mesmo conteúdo que aqueles que o possuam e tenham ativado.
